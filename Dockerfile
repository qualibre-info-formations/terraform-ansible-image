ARG VERSION=0.15.0
ARG OPENTOFU_VERSION=1.6.1

FROM registry.gitlab.com/components/opentofu/gitlab-opentofu:${VERSION}-opentofu${OPENTOFU_VERSION}

RUN apk add --no-cache python3 py3-pip py3-virtualenv

WORKDIR /py3-venv

RUN python3 -m venv venv

RUN . /py3-venv/venv/bin/activate && pip install ansible ovh
